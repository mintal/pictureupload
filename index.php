<?php require_once("resources/config.php"); ?>

<!DOCTYPE html>
<html>
<header>
<title>File Upload</title>
	<style>
	html, body{
	  height:100%;
	  padding:0;
	  margin:0;
	}
	.dz-preview {
		
		display:none !important;
		
	}
	
	.version {
		color:white;
		position:fixed;
		bottom:0;
		right:0;
	}
	</style>
</header>
	<body background="http://<?php echo $config["paths"]["images"]["layout"] ?>/bg.png">

	<form action="http://<?php echo $config["urls"]["baseUrl"] ?>/upload.php" method="post" id="myAwesomeDropzone" enctype="multipart/form-data" class="dropzone" style="width:100%;height:100%;">
		<input type="file" name="fileToUpload" style="visibility: hidden; position: absolute; top: 0px; left: 0px; height: 0px; width: 0px;">
		
	</form>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="http://<?php echo $config["urls"]["baseUrl"] ?>/js/dropzone.js"></script>
	<span class="version">
		<?php
		
		echo file_get_contents("version.txt");
		?>
	</span>
	</body>
</html>