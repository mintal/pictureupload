<?php

$host = "localhost";
$folder = "pictureupload";
$url = $host."/".$folder;
 
$config = array(
    "urls" => array(
        "baseUrl" => $url
    ),
    "paths" => array(
        "resources" => $url."/resources",
        "images" => array(
            "content" =>  $url."/img/content",
            "layout" =>  $url."/img/layout"
        )
    )
);


 
defined("LIBRARY_PATH")
    or define("LIBRARY_PATH", realpath(dirname(__FILE__) . '/library'));
     
defined("TEMPLATES_PATH")
    or define("TEMPLATES_PATH", realpath(dirname(__FILE__) . '/templates'));
 
/*
    Error reporting.
*/
ini_set("error_reporting", "true");
error_reporting(E_ALL|E_STRCT);

?>