<?php 

class readFiles {
	
	function PrintImage($dir){
		if (is_dir($dir)){
		  if ($dh = opendir($dir)){
			while (($file = readdir($dh)) !== false){
				if($file != "." && $file != ".." && $file != "index.php"){
					echo 
					"
					<div class='col-xs-6 col-md-3'>
						<a href='$dir$file' class='thumbnail'>
							<img src='$dir$file' alt='image'>
						</a>
					</div>
					";
				}
			}
			closedir($dh);
		  }
		}
	}
	
}