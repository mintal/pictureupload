<?php 
require_once('class_RNG.php');

class writeFile {
	private $EchoDebugging = false;
	
	function CheckFileSize($target_file){
		$check = getimagesize($target_file["tmp_name"]);
		if($check !== false){
			if($this->EchoDebugging == true)
				echo "file is an image :)";
			return true;
		} else {
			if($this->EchoDebugging == true)
				echo "file is not an image :(";
			return false;
		}
	}
	
	function maxFileSize($file){
		if($file["size"] > 500000){
			echo "sorry your file is too big";
			return false;
		} else {
			return true;
		}
	}
	
	function allowedFileExtensions($file){
		
		switch(pathinfo($file['name'], PATHINFO_EXTENSION)){
			case "png":
			case "gif":
			case "jpeg":
			case "jpg":
			return true;
			break;
			default:
			return false;
		}
		
	}
	
	function securityChecks($target_file){
		if(
		$this->CheckFileSize($target_file) == true && 
		$this->maxFileSize($target_file) == true &&
		$this->allowedFileExtensions($target_file) == true
		)
		return true;
	}
	
	function uploadFile($file,$target_dir){
		$rng = new RNG;
		if($this->securityChecks($file)){
			$dir = $target_dir.$rng->generateRNG(10).".".pathinfo($file['name'], PATHINFO_EXTENSION);
			if(move_uploaded_file($file['tmp_name'], $dir)){
				echo "your file ".basename($file["name"])." has been uploaded to: <a href='". $dir . "'>here</a>";
			} else {
				echo "something went wrong :(";
			}
		} else {
			echo "this is not a supported file.";
		}
	}
	
	
	
	
}